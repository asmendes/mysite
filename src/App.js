import React from 'react';
import Routes from './routes';

import "./css/style.css";
import "./css/contact/contact.css";
import "./css/main/main.css";
import "./css/service/service.css";
import "./css/team/team.css";

function App () {
  return (
    <div>
        <Routes />
    </div>    
  );
};

export default App;
