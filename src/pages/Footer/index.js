import React from 'react';

function Main(){
    return(
        <div>
            <nav>
                <footer class="page-footer #1a237e indigo darken-4">
                        <div class="container">
                        © Denselvolvido por MakMirror
                        <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
                        </div>
                </footer>
            </nav>
        </div>
    );
};

export default Main;