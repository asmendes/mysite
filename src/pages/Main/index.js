import React from 'react';


function Main(){
    return(
        <div class="my-site">
            <div class="navbar-fixed">
                <nav class="my-nav">
                    <div class="nav-wrapper #1a237e indigo darken-4">
                        <a href="#" class="brand-logo"><img width="160vw" src="img/logo.png"/></a>
                        <ul id="nav-mobile" class="right hide-on-med-and-down">
                            <li><a href="/">INICIO</a></li>
                            <li><a href="#servicos">SERVIÇOS</a></li>
                            <li><a href="#quem-somos">QUEM SOMOS</a></li>
                            <li><a href="#contato">CONTATO</a></li>
                        </ul>
                    </div>
                </nav>
            </div>

            <nav class="my-body nav-body">
                <section class="carousel">
                    <input type="checkbox" id="toggle"/>
                    <div class="paineis">
                        <article class="page1"><img src="img/banner/banner1.jpg" alt=""/></article>
                        <article class="page2"><img src="img/banner/banner2.png" alt=""/></article>
                        <article class="page3"><img src="img/banner/banner3.png" alt=""/></article>
                        <article class="page4"><img src="img/banner/banner4.jpg" alt=""/></article>
                        <article class="page5"><img src="img/banner/banner6.jpg" alt=""/></article>
                    </div>
                </section>
            </nav>

            <nav class="nav-body"  id="servicos">
                <div>
                    <div class="my-title-service">
                        <h2 class="my-title">SERVIÇOS</h2>
                    </div>

                    <div class="my-body-service">
                        <div class="my-service">
                            <img src="img/logo-contato.jpeg"/>
                            <h5>SERVIÇO 1</h5>
                        </div> <div class="my-service">
                            <img src="img/logo-contato.jpeg"/>
                            <h5>SERVIÇO 2</h5>
                        </div>
                        <div class="my-service">
                            <img src="img/logo-contato.jpeg"/>
                            <h5>SERVIÇO 3</h5>
                        </div>
                        <div class="my-service">
                            <img src="img/logo-contato.jpeg"/>
                            <h5>SERVIÇO 4</h5>
                        </div>
                    </div>


                </div>
            </nav>

            <nav  class="my-quem-somos nav-body" id="quem-somos">
                
                <div class="my-team">
                    <div class="my-titulo-quem-somos">
                        <h2 class="my-title">NOSSA EQUIPE</h2>
                    </div>

                    <div class="my-corpo-quem-somos my-text-team">
                        <div class="my-bloco-funcionario">
                            <img src="img/logo-contato.jpeg"/>
                            <h5>NOME SOBRENOME</h5>
                            <p>Breve Descrição sobre formação e contrubuição</p>
                        </div>
                        <div class="my-bloco-funcionario">
                            <img src="img/logo-contato.jpeg"/>
                            <h5>NOME SOBRENOME</h5>
                            <p>Breve Descrição sobre formação e contrubuição</p>
                        </div>
                        <div class="my-bloco-funcionario">
                            <img src="img/logo-contato.jpeg"/>
                            <h5>NOME SOBRENOME</h5>
                            <p>Breve Descrição sobre formação e contrubuição</p>
                        </div>
                    </div>

                    <div class="my-description-team my-text-team">
                        Breve descrição sobre a empresa e como desejamos atuar dentro do mercado e na vida
                        de nossos clientes. Trabalhando com os concluídos,mas principalmente com a satisfação
                        de nossos cliente.
                    </div>

                </div>
            </nav>

            <nav class="my-contato nav-body #e0e0e0 grey lighten-2" id="contato">
                <div class="my-form">
                    <form>
                        <h1>Fale Conosco</h1>
                        <input type="text" placeholder="Nome"></input>
                        <input type="text" placeholder="Assunto"></input>
                        <input type="text" placeholder="E-mail"></input>
                        <input type="text" placeholder="WhatsApp"></input>
                        <input type="textarea" placeholder="Deixe sua mensagem aqui"></input>
                        <button class="btn #1a237e indigo darken-4">Enviar</button>
                    </form>
                </div>
                <div class="my-img-contato">
                    <img class="redes" src="img/redes2.png"/>
                    <img class="logo-contato" src="img/logo-contato.jpeg"/>
                </div>
            </nav>

            <nav class="my-footer">
                <footer class="page-footer #1a237e indigo darken-4">
                        <div class="container">
                        © Denselvolvido por MakMirror
                        <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
                        </div>
                </footer>
            </nav>
        </div>
    );
};

export default Main;