import React from 'react';

function Main(){
    return(
        <div>
            <nav>
                <div class="nav-wrapper #1a237e indigo darken-4">
                    <a href="#" class="brand-logo"><img width="160vw" src="img/logo.png"/></a>
                    <ul id="nav-mobile" class="right hide-on-med-and-down">
                        <li><a href="sass.html">INICIO</a></li>
                        <li><a href="badges.html">SERVIÇOS</a></li>
                        <li><a href="collapsible.html">QUEM SOMOS</a></li>
                        <li><a href="collapsible.html">CONTATO</a></li>
                     </ul>
                </div>
            </nav>
        </div>
    );
};

export default Main;