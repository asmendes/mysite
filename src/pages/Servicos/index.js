import React from 'react';

function Servicos(){
    return(
        <div>
            <nav class="my-nav">
                <div class="nav-wrapper #1a237e indigo darken-4">
                    <a href="#" class="brand-logo"><img width="160vw" src="img/logo.png"/></a>
                    <ul id="nav-mobile" class="right hide-on-med-and-down">
                        <li><a href="/">INICIO</a></li>
                        <li><a href="/servicos">SERVIÇOS</a></li>
                        <li><a href="/quem-somos">QUEM SOMOS</a></li>
                        <li><a href="/contato">CONTATO</a></li>
                     </ul>
                </div>
            </nav>

            <nav class="my-quem-somos #e0e0e0 grey lighten-2">
            
                
            </nav>

            <nav class="my-footer">
                <footer class="page-footer #1a237e indigo darken-4">
                        <div class="container">
                        © Denselvolvido por MakMirror
                        <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
                        </div>
                </footer>
            </nav>
        </div>
    );
};

export default Servicos;