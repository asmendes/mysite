import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';

import Main from './pages/Main';
import QuemSomos from './pages/QuemSomos';
import Contato from './pages/Contato';
import Servicos from './pages/Servicos';

function Routes(){
    return(
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={Main} />
                <Route path="/servicos" component={Servicos} />
                <Route path="/quem-somos" component={QuemSomos} />
                <Route path="/contato" component={Contato} />
            </Switch>        
        </BrowserRouter>
    );
};

export default Routes;